# Generated by Django 2.2.5 on 2019-11-29 19:41

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('polls', '0002_auto_20191128_2215'),
    ]

    operations = [
        migrations.CreateModel(
            name='Opponent_Pokemon',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50)),
                ('ability', models.CharField(blank=True, max_length=50, null=True)),
                ('item', models.CharField(blank=True, max_length=50, null=True)),
                ('base_stats', models.CharField(blank=True, max_length=50, null=True)),
                ('dimax', models.BooleanField(default=False)),
                ('moves_1', models.CharField(blank=True, max_length=50, null=True)),
                ('moves_2', models.CharField(blank=True, max_length=50, null=True)),
                ('moves_3', models.CharField(blank=True, max_length=50, null=True)),
                ('moves_4', models.CharField(blank=True, max_length=50, null=True)),
                ('type_1', models.CharField(blank=True, max_length=50, null=True)),
                ('type_2', models.CharField(blank=True, max_length=50, null=True)),
                ('hp_percrnt', models.FloatField(default=100)),
                ('hp', models.FloatField(default=0)),
                ('attack', models.IntegerField(default=0)),
                ('defense', models.IntegerField(default=0)),
                ('special_attack', models.IntegerField(default=0)),
                ('special_defense', models.IntegerField(default=0)),
                ('speed', models.IntegerField(default=0)),
                ('attack_boost', models.IntegerField(default=0)),
                ('defense_boost', models.IntegerField(default=0)),
                ('special_attack_boost', models.IntegerField(default=0)),
                ('special_defense_boost', models.IntegerField(default=0)),
                ('speed_boost', models.IntegerField(default=0)),
            ],
        ),
        migrations.RenameField(
            model_name='pokemon',
            old_name='special_atack',
            new_name='special_attack',
        ),
    ]
