# Generated by Django 2.2.5 on 2019-12-07 20:06

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('polls', '0006_auto_20191204_0533'),
    ]

    operations = [
        migrations.CreateModel(
            name='Convert_Moves',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('japanese_name', models.CharField(blank=True, max_length=50, null=True)),
                ('english_name', models.CharField(blank=True, max_length=50, null=True)),
            ],
        ),
    ]
