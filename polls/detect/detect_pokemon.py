import numpy as np
import matplotlib.pyplot as plt
import torch
from torch import nn
from torch import optim
from torch.utils.data import TensorDataset, DataLoader
from torchvision.datasets import ImageFolder
from torchvision import transforms, models, utils
import pretrainedmodels
from torchsummary import summary
from tqdm import tqdm
from PIL import Image

# モデルの定義(2層と5層に残差接続とSeModule追加)
num_classes = 104  # 出力クラス数
from pylab import rcParams
import numpy as np
import matplotlib.pyplot as plt
import pyautogui
import os
import time
import cv2
from PIL import Image

class AlexNet(nn.Module):

    # 畳み込みブロック(プーリングなし)
    def conv_block(self, in_dim, out_dim):
        return nn.Sequential(
            nn.Conv2d(in_dim, out_dim, kernel_size=3, stride=1, padding=1),
            nn.BatchNorm2d(out_dim),
            nn.ReLU(inplace=True),
            nn.Conv2d(out_dim, out_dim, kernel_size=3, stride=1, padding=1),
            nn.BatchNorm2d(out_dim),
            nn.ReLU(inplace=True),
        )

    # 畳み込みブロック(プーリングあり)
    def conv_block_pooling(self, in_dim, out_dim):
        return nn.Sequential(
            nn.Conv2d(in_dim, out_dim, kernel_size=3, stride=1, padding=1),
            nn.BatchNorm2d(out_dim),
            nn.ReLU(inplace=True),
            nn.MaxPool2d(kernel_size=2),
        )

    # SeModule
    def semodule(self, channels):
        return nn.Sequential(
            nn.AdaptiveAvgPool2d(1),
            nn.Conv2d(channels, channels // 8, kernel_size=1, stride=1, padding=0),
            nn.ReLU(inplace=True),
            nn.Conv2d(channels // 8, channels, kernel_size=1, stride=1, padding=0),
            nn.ReLU(inplace=True),
        )

    def __init__(self, num_classes):
        super(AlexNet, self).__init__()
        # 畳み込みブロック群
        self.conv_block1 = self.conv_block_pooling(3, 96)

        self.residual1 = nn.Conv2d(96, 256, kernel_size=1, stride=1, padding=0)  # チャンネルサイズ調整(Pointwise Convolution)
        self.conv_block2 = self.conv_block(96, 256)
        self.seblock = self.semodule(256)

        self.conv_block3 = self.conv_block_pooling(256, 384)

        self.conv_block4 = self.conv_block(384, 384)

        self.residual2 = nn.Conv2d(384, 256, kernel_size=1, stride=1, padding=0)
        self.conv_block5 = self.conv_block(384, 256)

        self.gap = nn.AdaptiveAvgPool2d(1)
        # 全結合ブロック群
        self.classifier = nn.Sequential(
            # 6ブロック(ここから全結合層)
            nn.Dropout(p=0.5),
            nn.Linear(256, 1048),
            # 7ブロック
            nn.Dropout(p=0.5),
            nn.Linear(1048, 1048),
            # 8ブロック(出力)
            nn.Linear(1048, num_classes),
        )

    # モデルの定義
    def forward(self, x):
        # 1-5ブロック
        x = self.conv_block1(x)

        residual = self.residual1(x)
        x = self.conv_block2(x)
        x = x + residual  # 残差接続(チャンネル数、サイズ同じ、足す)
        se = self.seblock(x)
        x = se * x  # SeModule追加(各チャンネル毎に重みを掛ける)

        x = self.conv_block3(x)
        x = self.conv_block4(x)

        residual = self.residual2(x)
        x = self.conv_block5(x)
        x = x + residual
        se = self.seblock(x)
        x = se * x

        # Global Average Pooling
        x = self.gap(x)
        x = x.view(-1, 256)
        # 6-8ブロック
        x = self.classifier(x)
        return x

class Detect():
    def screen_shot(self,yset, ofset_y=0, ofset_x=0):
        images = []
        for i in range(0, 6):
            a = yset[i]
            images.append(pyautogui.screenshot(region=(1230 + ofset_x, a + 210 + 120 * i + ofset_y, 90, 90)))

        return images


    def detect_pokemon(self):
        f = open(r"C:\Users\kohei nakanishi\hello\polls\detect\list.txt", "r")
        dict = []

        for x in f:
            dict.append(x.rstrip("\n"))
        f.close()
        num_classes=len(dict)
        device = 'cuda' if torch.cuda.is_available() else 'cpu'
        net = AlexNet(num_classes).to(device)  # モデルをインスタンス
        # 学習済みモデルを使用する場合はモデルをロード
        net.load_state_dict(torch.load(r"C:\Users\kohei nakanishi\hello\polls\detect\net3.ckpt"))

        yset = [0, 0, 0, 0, 0, 0]
        image = self.screen_shot(yset, -10, -5)
        for i in range(0, 6):
            image[i] = transforms.ToTensor()(image[i])
            image[i] = image[i].unsqueeze(0)
            if not i == 0:
                image[0] = torch.cat((image[0], image[i]), dim=0)


        result=[]
        net.eval()
        with torch.no_grad():
            output = net(image[0].cuda())
            output = output.cpu()

            predict_label = output.max(1)[1]
            for i in predict_label:
                result.append(dict[i.item()])

        return result
