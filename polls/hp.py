import cv2 as cv
import numpy as np
import matplotlib.pyplot as plt

from PIL import ImageGrab, Image
import sys

import pyocr
import pyocr.builders
import io

from google.cloud import vision
import os
from pathlib import Path
os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = r'C:\Users\kohei nakanishi\hello\static\js\pokemon-70ba915574fb.json'


def image_to_byte_array(image: Image):
    imgByteArr = io.BytesIO()
    image.save(imgByteArr, format=image.format)
    imgByteArr = imgByteArr.getvalue()
    return imgByteArr


class Hp():
    def __init__(self):
        self.image = ImageGrab.grabclipboard()

    def detect_hp(self, image):
        # HPバー求める
        # ファイルを読み込み
        src = np.asarray(image)
        # 画像の大きさ取得
        height, width, channels = src.shape

        image_size = height * width
        # グレースケール化
        img_gray = cv.cvtColor(src, cv.COLOR_RGB2GRAY)

        # しきい値指定によるフィルタリング
        retval, dst = cv.threshold(img_gray, 108, 255, cv.THRESH_TOZERO_INV)
        # 白黒の反転
        dst = cv.bitwise_not(dst)
        # 再度フィルタリング
        retval, dst = cv.threshold(dst, 0, 255, cv.THRESH_BINARY | cv.THRESH_OTSU)

        # 輪郭を抽出
        dst, contours, hierarchy = cv.findContours(dst, cv.RETR_TREE, cv.CHAIN_APPROX_SIMPLE)
        data = []
        for i, contour in enumerate(contours):
            # 小さな領域の場合は間引く
            area = cv.contourArea(contour)
            if area < 0:
                continue
            # 画像全体を占める領域は除外する
            if image_size * 0.98 < area:
                continue

            # 外接矩形を取得
            x, y, w, h = cv.boundingRect(contour)
            a = [x, y, w, h]
            data.append(a)
            dst = cv.rectangle(dst, (x, y), (x + w, y + h), (0, 255, 0), 2)

        a = data[0][2] / (width)

        return str(round(a * 100))

    def detect_name(self, image):
        """
        tools = pyocr.get_available_tools()
        if len(tools) == 0:
            print("No OCR tool found")
            sys.exit(1)
        # The tools are returned in the recommended order of usage
        tool = tools[0]
        txt = tool.image_to_string(
            image,
            lang='jpn',
            builder=pyocr.builders.TextBuilder(tesseract_layout=8)
        )

        name = ''
        for i in txt:
            if (i != '@') & (i != '⑳') & (i != ' ') & (i != '　') & (i != '④') & (i != '②'):
                name = name + i
            elif (i == '@') | (i == '⑳') | (i == '④') | (i == '②'):
                break

        return name

        :param image:
        :return:
        """
        client = vision.ImageAnnotatorClient()
        image.save('polls/2.jpg')
        p = Path(__file__).parent / '2.jpg'
        with p.open('rb') as image_file:
            content = image_file.read()
        image = vision.types.Image(content=content)
        #image = vision.types.Image(content=image_to_byte_array(image))
        response = client.text_detection(image=image)
        for text in response.text_annotations:
            return text.description

    def detect_n(self, image):
        # HPバー求める
        # ファイルを読み込み
        src = np.asarray(image)
        # 画像の大きさ取得
        height = src.shape[0]
        width = src.shape[1]

        image_size = height * width
        # グレースケール化
        img_gray = cv.cvtColor(src, cv.COLOR_RGB2GRAY)
        # print(img_gray)
        # しきい値指定によるフィルタリング
        retval, dst = cv.threshold(img_gray, 255, 255, cv.THRESH_TOZERO_INV)
        dst = cv.bitwise_not(dst)
        # 再度フィルタリング
        # retval, dst = cv.threshold(dst, 0, 255, cv.THRESH_BINARY | cv.THRESH_OTSU)
        plt.imshow(dst)
        pilImg = Image.fromarray(dst)
        return self.detect_name(pilImg)
