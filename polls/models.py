from django.db import models


# Create your models here.
class Pokemon(models.Model):
    team_id = models.IntegerField(blank=True, null=True)
    name = models.CharField(blank=False, max_length=50)
    ability = models.CharField(blank=True, null=True, max_length=50)
    item = models.CharField(blank=True, null=True, max_length=50)
    base_stats = models.CharField(blank=True, null=True, max_length=50)  # 性格
    dimax = models.BooleanField(default=False)
    moves_1 = models.CharField(blank=True, null=True, max_length=50)
    moves_2 = models.CharField(blank=True, null=True, max_length=50)
    moves_3 = models.CharField(blank=True, null=True, max_length=50)
    moves_4 = models.CharField(blank=True, null=True, max_length=50)
    type_1 = models.CharField(blank=True, null=True, max_length=50)
    type_2 = models.CharField(blank=True, null=True, max_length=50)
    hp_percent = models.CharField(blank=True, null=True, max_length=50, default="100")
    hp = models.IntegerField(default=0)
    attack = models.IntegerField(default=0)
    defense = models.IntegerField(default=0)
    special_attack = models.IntegerField(default=0)
    special_defense = models.IntegerField(default=0)
    speed = models.IntegerField(default=0)
    attack_boost = models.IntegerField(default=0)
    defense_boost = models.IntegerField(default=0)
    special_attack_boost = models.IntegerField(default=0)
    special_defense_boost = models.IntegerField(default=0)
    speed_boost = models.IntegerField(default=0)

    def __str__(self):
        return self.name


class Opponent_Pokemon(models.Model):
    kata = models.CharField(blank=True, max_length=100)
    name = models.CharField(blank=False, max_length=50)
    ability = models.CharField(blank=True, null=True, max_length=50)
    item = models.CharField(blank=True, null=True, max_length=50)
    base_stats = models.CharField(blank=True, null=True, max_length=50)  # 性格
    dimax = models.BooleanField(default=False)
    moves_1 = models.CharField(blank=True, null=True, max_length=50)
    moves_2 = models.CharField(blank=True, null=True, max_length=50)
    moves_3 = models.CharField(blank=True, null=True, max_length=50)
    moves_4 = models.CharField(blank=True, null=True, max_length=50)
    type_1 = models.CharField(blank=True, null=True, max_length=50)
    type_2 = models.CharField(blank=True, null=True, max_length=50)
    hp_percent = models.CharField(blank=True, null=True, max_length=50, default="100")
    hp = models.IntegerField(default=0)
    attack = models.IntegerField(default=0)
    defense = models.IntegerField(default=0)
    special_attack = models.IntegerField(default=0)
    special_defense = models.IntegerField(default=0)
    speed = models.IntegerField(default=0)
    attack_boost = models.IntegerField(default=0)
    defense_boost = models.IntegerField(default=0)
    special_attack_boost = models.IntegerField(default=0)
    special_defense_boost = models.IntegerField(default=0)
    speed_boost = models.IntegerField(default=0)

    def __str__(self):
        return self.name

class Kata_Pokemon(models.Model):
    kata=models.CharField(blank=False, max_length=100)
    name = models.CharField(blank=False, max_length=50)
    ability = models.CharField(blank=True, null=True, max_length=50)
    item = models.CharField(blank=True, null=True, max_length=50)
    base_stats = models.CharField(blank=True, null=True, max_length=50)  # 性格
    dimax = models.BooleanField(default=False)
    moves_1 = models.CharField(blank=True, null=True, max_length=50)
    moves_2 = models.CharField(blank=True, null=True, max_length=50)
    moves_3 = models.CharField(blank=True, null=True, max_length=50)
    moves_4 = models.CharField(blank=True, null=True, max_length=50)
    hp_percent = models.CharField(blank=True, null=True, max_length=50, default="100")
    hp = models.IntegerField(default=0)
    attack = models.IntegerField(default=0)
    defense = models.IntegerField(default=0)
    special_attack = models.IntegerField(default=0)
    special_defense = models.IntegerField(default=0)
    speed = models.IntegerField(default=0)
    attack_boost = models.IntegerField(default=0)
    defense_boost = models.IntegerField(default=0)
    special_attack_boost = models.IntegerField(default=0)
    special_defense_boost = models.IntegerField(default=0)
    speed_boost = models.IntegerField(default=0)

    def __str__(self):
        return self.name+"/"+self.kata


class Convert_Name(models.Model):
    pokemon_id = models.CharField(blank=True, null=True, max_length=50)
    japanese_name = models.CharField(blank=True, null=True, max_length=50)
    english_name = models.CharField(blank=True, null=True, max_length=50)


class Convert_Moves(models.Model):
    japanese_name = models.CharField(blank=True, null=True, max_length=50)
    english_name = models.CharField(blank=True, null=True, max_length=50)
    dimax_power = models.IntegerField(default=0)


class Convert_Item(models.Model):
    japanese_name = models.CharField(blank=True, null=True, max_length=50)
    english_name = models.CharField(blank=True, null=True, max_length=50)


class Convert_Tokusei(models.Model):
    japanese_name = models.CharField(blank=True, null=True, max_length=50)
    english_name = models.CharField(blank=True, null=True, max_length=50)


class Convert_Seikaku(models.Model):
    japanese_name = models.CharField(blank=True, null=True, max_length=50)
    english_name = models.CharField(blank=True, null=True, max_length=50)

class Conditions(models.Model):
    weather=models.CharField(blank=True, null=True, max_length=50)
    terrain=models.CharField(blank=True, null=True, max_length=50)
    reflect= models.BooleanField(default=False)
    light_screen=models.BooleanField(default=False)
    aurora_veil=models.BooleanField(default=False)