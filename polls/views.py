from django.shortcuts import render
import math

# Create your views here.

from . import hp
from .detect import detect_pokemon

from .models import Pokemon
from .models import Opponent_Pokemon
import csv
import pyautogui

import sys

sys.path.append(r'C:\Users\kohei nakanishi\Documents\強化学習-20191115T060808Z-001\showdown-master\tests')

from .showdown.showdown.damage_calculator import calculate_damage
from .showdown.showdown.engine.objects import Pokemon as Pokemon1
from .showdown.showdown.battle import Pokemon as StatePokemon
from .showdown import constants

from .models import Convert_Name
from .models import Convert_Moves
from .models import Convert_Item
from .models import Convert_Tokusei
from .models import Convert_Seikaku
from .models import Kata_Pokemon

from .showdown.showdown.helpers import calculate_stats
import re


def index(request):
    if request.method == 'POST':
        if 'button_1' in request.POST:
            """
            pgui.alert('取りたいウインドウをクリックしてください。')
            pgui.hotkey('win', 'shift', 's')
            """

            with open('../static/js/sample.csv') as f:
                reader = csv.reader(f)
                for row in reader:
                    print(row)

        elif 'button_2' in request.POST:
            a = hp.Hp()
            name = a.detect_name()
            hitpoints = math.ceil((a.detect_hp() * 100))
            print(hitpoints)

            d = {
                'poke': name,
                'HP': str(hitpoints) + '%',
            }
            return render(request, 'result.html', d)
        """
        elif 'button_3' in request.POST:
            a = detect_pokemon.Detect()
            name = a.detect_pokemon()
            e = {
                'a': name[0],
                'b': name[1],
                'c': name[2],
                'd': name[3],
                'e': name[4],
                'f': name[5],
            }
            return render(request, 'result_name.html', e)
        print(request.POST.get('g1'))
        """
    return render(request, 'index.html')


def new_status(my_pokemon):
   # print(my_pokemon)
    my_pokemon_status = Pokemon1.from_state_pokemon_dict(StatePokemon(convert_name(my_pokemon.name), 50).to_dict())
    my_pokemon_status.attack_boost = int(my_pokemon.attack_boost)
    my_pokemon_status.defense_boost = int(my_pokemon.defense_boost)
    my_pokemon_status.special_attack_boost = int(my_pokemon.special_attack_boost)
    my_pokemon_status.special_defense_boost = int(my_pokemon_status.special_defense_boost)
    my_pokemon_status.speed_boost = int(my_pokemon.speed_boost)
    if not my_pokemon.item == None:
        my_pokemon_status.item = convert_item(my_pokemon.item)
    if not my_pokemon.ability == None:
        my_pokemon_status.ability = convert_tokusei(my_pokemon.ability)
    my_new_status = calculate_stats(my_pokemon_status.base_stats, my_pokemon_status.level,
                                    evs=[int(my_pokemon.hp), int(my_pokemon.attack), int(my_pokemon.defense),
                                         int(my_pokemon.special_attack), int(my_pokemon.special_defense),
                                         int(my_pokemon.speed)],
                                    nature=convert_seikaku(my_pokemon.base_stats))
    if my_pokemon.dimax:
        my_pokemon_status.hp = my_new_status[constants.HITPOINTS] * 2
    else:
        my_pokemon_status.hp = my_new_status[constants.HITPOINTS]
    my_pokemon_status.attack = my_new_status[constants.ATTACK]
    my_pokemon_status.defense = my_new_status[constants.DEFENSE]
    my_pokemon_status.special_attack = my_new_status[constants.SPECIAL_ATTACK]
    my_pokemon_status.special_defense = my_new_status[constants.SPECIAL_DEFENSE]
    my_pokemon_status.speed = my_new_status[constants.SPEED]
    # my_pokemon_status.calculate_boosted_stats()
    return my_pokemon_status


def get_damage_percent(hp, damages):
    if damages == None:
        return "ダメージなし"
    maxs = (max(damages) / hp) * 100
    mins = (min(damages) / hp) * 100
    kakuteisu = []
    for damage in damages:
        if damage == 0:
            return "ダメージなし"
        a = math.ceil(hp / damage)
        kakuteisu.append(a)

    min_kakuteisu = min(kakuteisu)
    same_kakuteisu = 0
    else_kakuteisu = 0
    for i in kakuteisu:
        if min_kakuteisu == i:
            same_kakuteisu = same_kakuteisu + 1
        else:
            else_kakuteisu = else_kakuteisu + 1
    # print(same_kakuteisu,else_kakuteisu)
    kakutei_percent = round((same_kakuteisu / (same_kakuteisu + else_kakuteisu)) * 100, 1)
    k = ""
    if kakutei_percent == 100:
        k = "確定"
    else:
        k = "乱数"
    return str(round(mins, 1)) + "%~" + str(round(maxs, 1)) + "%" + k + str(min_kakuteisu) + "発(" + str(
        kakutei_percent) + "%)"


def calculate_damages(my_pokemon, opponent_pokemons, condition=None):
    new_my_pokemon = new_status(my_pokemon)
    opponent_pokemons_damages = []
    for opponent_pokemon in opponent_pokemons:
        new_opponent_pokemon = new_status(opponent_pokemon)
        damages = []
        if not my_pokemon.moves_1 == None:
            damages.append(get_damage_percent(new_opponent_pokemon.hp,
                                              calculate_damage(new_my_pokemon, new_opponent_pokemon,
                                                               convert_moves(my_pokemon.moves_1), calc_type='all',
                                                               conditions=condition, dimax=my_pokemon.dimax)))
        else:
            damages.append("None")

        if not my_pokemon.moves_2 == None:
            damages.append(get_damage_percent(new_opponent_pokemon.hp,
                                              calculate_damage(new_my_pokemon, new_opponent_pokemon,
                                                               convert_moves(my_pokemon.moves_2), calc_type='all',
                                                               conditions=condition, dimax=my_pokemon.dimax)))
        else:
            damages.append("None")

        if not my_pokemon.moves_3 == None:
            damages.append(get_damage_percent(new_opponent_pokemon.hp,
                                              calculate_damage(new_my_pokemon, new_opponent_pokemon,
                                                               convert_moves(my_pokemon.moves_3), calc_type='all',
                                                               conditions=condition, dimax=my_pokemon.dimax)))
        else:
            damages.append("None")

        if not my_pokemon.moves_4 == None:
            damages.append(get_damage_percent(new_opponent_pokemon.hp,
                                              calculate_damage(new_my_pokemon, new_opponent_pokemon,
                                                               convert_moves(my_pokemon.moves_4), calc_type='all',
                                                               conditions=condition, dimax=my_pokemon.dimax)))
        else:
            damages.append("None")
        opponent_pokemons_damages.append(damages)
        # print(damages)
    return opponent_pokemons_damages


def top(request):
    if request.method == 'POST':
        if 'button_1' in request.POST:
            """
            pgui.alert('取りたいウインドウをクリックしてください。')
            pgui.hotkey('win', 'shift', 's')
           

            charizard = Pokemon1.from_state_pokemon_dict(StatePokemon("charizard", 50).to_dict())  # 100レベル
            venusaur = Pokemon1.from_state_pokemon_dict(StatePokemon("venusaur", 50).to_dict())

            new_stats = calculate_stats(charizard.base_stats, charizard.level, evs=[0, 0, 0, 252, 0, 252])

            charizard.attack = new_stats[constants.ATTACK]

            charizard.special_attack = new_stats[constants.SPECIAL_ATTACK]

            print(charizard)
            move = 'Fire Blast'

            dmg = calculate_damage(charizard, venusaur, move, calc_type='all')
            print(get_damage_percent(venusaur.maxhp, dmg))
             """

            """
            
            """
            load_kata_data()
            # with open(r'C:\Users\kohei nakanishi\hello\static\js\c2.csv', encoding="utf-8-sig") as f:
            # reader = csv.reader(f)
            # for row in reader:
            # if not row[0]=="\ufeff":
            # a = Convert_Moves.objects.get(japanese_name=row[0])
            # a.dimax_power = int(row[1])
            # a.save()
    """ 
    if request.method == 'POST':
        if 'button_3' in request.POST:
            pokemon = Pokemon.objects.all()
            e = {
                'pokemon': pokemon[0],
                'p1': pokemon[0].name,
                'p2': pokemon[1].name,
                'p3': pokemon[2].name,
                'p4': pokemon[3].name,
                'p5': pokemon[4].name,
                'p6': pokemon[5].name,
            }
        return redirect(request, 'http://127.0.0.1:8000/damage', e)
    """
    return render(request, 'top.html')


num = {0: "0", 1: "1", 2: "2", 3: "3", 4: "4", 5: "5"}


def detect_op_pokemon(request, type="防御"):
    opponent_pokemon = []
    names = []
    katas = []
    kata_index = []
    all = Opponent_Pokemon.objects.all()
    for i in all:
        if not i.name in names:
            names.append(i.name)

    if type == "攻撃":
        field_names = []
        for i in range(0, 6):
            field_names.append(request.POST.get(num.get(i)))

        for i in names:
            kata = []
            if not i in field_names:
                Opponent_Pokemon.objects.filter(name=i).delete()
                for k in field_names:
                    if not k in names:
                        targets = Kata_Pokemon.objects.filter(name=k)
                        for target in targets:
                            Opponent_Pokemon.objects.create(name=target.name, kata=target.kata, ability=target.ability,
                                                            item=target.item,
                                                            base_stats=target.base_stats,
                                                            dimax=False, moves_1=target.moves_1, moves_2=target.moves_2,
                                                            moves_3=target.moves_3, moves_4=target.moves_4,
                                                            hp_percent="100",
                                                            hp=target.hp, attack=target.attack,
                                                            defense=target.defense,
                                                            special_attack=target.special_attack,
                                                            special_defense=target.special_defense,
                                                            speed=target.speed, attack_boost=target.attack_boost,
                                                            defense_boost=target.defense_boost,
                                                            special_attack_boost=target.special_attack_boost,
                                                            special_defense_boost=target.special_defense_boost,
                                                            speed_boost=target.speed_boost)
                            kata.append(target.kata)
                        katas.append(kata)
                        a = Opponent_Pokemon.objects.filter(name=k)[0]
                        opponent_pokemon.append(a)
                        kata_index.append(a.kata)
            else:
                a = Opponent_Pokemon.objects.get(name=i, kata=request.POST.get("kata_" + i))
                opponent_pokemon.append(a)
                kata_index.append(a.kata)
                b = Opponent_Pokemon.objects.filter(name=i)
                for k in b:
                    kata.append(k.kata)
                katas.append(kata)

    else:
        for i in names:
            a = Opponent_Pokemon.objects.filter(name=i)
            opponent_pokemon.append(a[0])
            kata_index.append(a[0].kata)
            kata = []
            for i in a:
                kata.append(i.kata)
            katas.append(kata)

    return opponent_pokemon, kata_index, katas


def decide(request):
    condition = {"weather": "なし", "terrain": "なし", "reflect": False, "light_screen": False,
                 "aurora_veil": False, "wheather_japanese": "天候なし", "terrain_japanese": "フィールドなし"}
    if request.method == 'POST':
        role = request.POST.get("ROLE")

        id = request.POST.get("field")
        # target = Pokemon.objects.get(name=id)
        opponent_pokemon, kata_index, katas = detect_op_pokemon(request, role)
        change_defence = False
        change_attack = False
        if role == "攻撃(変更)":
            role = "攻撃"
            target = Pokemon.objects.filter(team_id=1)[0]
            change_attack =True

        if role == "防御(変更)":
            role = "防御"
            name = request.POST.get("0")
            target = Opponent_Pokemon.objects.get(name=name, kata=request.POST.get("kata_" + name))
            change_defence = True
        if role == "攻撃":
            if not change_attack:
                target = Pokemon.objects.get(name=id)
            attack_pokemon = Pokemon.objects.filter(team_id=1)
            defence_pokemon = opponent_pokemon
            kata_name=katas
            print(katas)
            kata_index=kata_index
            attack_kata_name=["味方"]
            attack_kata_index="味方"



        if role == "防御":
            if not change_defence and not "button_1" in request.POST:
                target = Opponent_Pokemon.objects.get(name=id, kata=request.POST.get("attack_kata"))
            elif not change_defence and "button_1" in request.POST:
                target = Opponent_Pokemon.objects.filter(name=id)[0]
            attack_pokemon = opponent_pokemon
            defence_pokemon = Pokemon.objects.filter(team_id=1)

            for i in range(0,6):
                if opponent_pokemon[i].name==target.name:
                    index=i
            attack_kata_name = katas[index]
            print(kata_index)
            attack_kata_index = target.kata

            kata_name = [["味方"],["味方"],["味方"],["味方"],["味方"],["味方"]]
            kata_index = ["味方","味方","味方","味方","味方","味方"]


        condition["weather"] = request.POST.get("weather")
        condition["terrain"] = request.POST.get("terrain")
        condition["reflect"] = constants.CONVERT_BOOL.get(request.POST.get("reflect"))
        condition["light_screen"] = constants.CONVERT_BOOL.get(request.POST.get("light_screen"))
        condition["aurora_veil"] = constants.CONVERT_BOOL.get(request.POST.get("aurora_veil"))
        condition["wheather_japanese"] = constants.CONVERT_WHEATHER.get(condition.get("weather"))
        condition["terrain_japanese"] = constants.CONVERT_TERRAIN.get(condition.get("terrain"))
        if 'button_2' in request.POST:
            #print(request.POST)
            target = save_attack_pokemon(request, target)
            for name in defence_pokemon:
                save_defence_pokemon(request, name)

        if 'button_3' in request.POST:
            hps = hp.Hp()
            img = pyautogui.screenshot(region=(20, 930, 274 - 20, 964 - 922))
            name = hps.detect_name(img)
            #print(name)
            l = ""
            for i in range(0, 5):
                lists = []
                for a in attack_pokemon:
                    if name.startswith(a.name[0:i]):
                        lists.append(a.name)
                if len(lists) == 1:
                    l = lists[0]
                    break
            if l == "":
                name = hps.detect_n(img)
                lists = []
                for a in attack_pokemon:
                    if name.startswith(a.name[0:i]):
                        lists.append(a.name)
                    if len(lists) == 1:
                        l = lists[0]
                        break
            for i in range(0, 6):
                if attack_pokemon[i].name == l:
                    attack_pokemon[i].hp_percent = hps.detect_hp(
                        pyautogui.screenshot(region=(25, 983, 425 - 27, 993 - 982)))
                    attack_pokemon[i].save()
                    target = attack_pokemon[i]

            img = pyautogui.screenshot(region=(1450, 40, 1706 - 1450, 92 - 40))
            name = hps.detect_name(img)

            l = ""
            for i in range(0, 5):
                lists = []
                for a in defence_pokemon:
                    if name.startswith(a.name[0:i]):
                        lists.append(a.name)
                if len(lists) == 1:
                    l = lists[0]
                    break

            if l == "":
                name = hps.detect_n(img)
                lists = []
                for a in defence_pokemon:
                    if name.startswith(a.name[0:i]):
                        lists.append(a.name)
                    if len(lists) == 1:
                        l = lists[0]
                        break

            for i in range(0, 6):
                if defence_pokemon[i].name == l:
                    defence_pokemon[i].hp_percent = hps.detect_hp(
                        pyautogui.screenshot(region=(1468, 95, 1867 - 1455 - 12, 112 - 100)))
                    defence_pokemon[i].save()
        damages = calculate_damages(target, defence_pokemon, condition)

        dimax = []
        dimax.append(target.dimax)
        for i in defence_pokemon:
            dimax.append(i.dimax)
        d = {
            'pokemon': target,
            'p1': attack_pokemon[0],
            'p2': attack_pokemon[1],
            'p3': attack_pokemon[2],
            'p4': attack_pokemon[3],
            'p5': attack_pokemon[4],
            'p6': attack_pokemon[5],
            'op1': defence_pokemon[0],
            'op2': defence_pokemon[1],
            'op3': defence_pokemon[2],
            'op4': defence_pokemon[3],
            'op5': defence_pokemon[4],
            'op6': defence_pokemon[5],
            '1d': damages[0],
            '2d': damages[1],
            '3d': damages[2],
            '4d': damages[3],
            '5d': damages[4],
            '6d': damages[5],
            'role': role,
            'condition': condition,
            'dimax': dimax,
            'kata_name': kata_name,
            'kata_index': kata_index,
            'attack_kata_name':attack_kata_name ,
            'attack_kata_index': attack_kata_index
        }
        return render(request, 'battle.html', d)

    pokemon = Pokemon.objects.filter(team_id=1)

    for p in pokemon:
        p.hp_percent = "100"
        p.dimax = False
        p.save()
    a = detect_pokemon.Detect()
    names = a.detect_pokemon()
    opponet_pokemons = []

    Opponent_Pokemon.objects.all().delete()
    for name in names:
        opponet_pokemons.append(detect_opponent_pokemon(name))

    opponet_pokemon = []
    kata_name = []
    kata_index = []
    for i in range(0, 6):
        opponet_pokemon.append(opponet_pokemons[i][0])
        kata_name.append(opponet_pokemons[i][1])
    for i in range(0, 6):
        kata_index.append(kata_name[i][0])
    damages = calculate_damages(pokemon[0], opponet_pokemon)

    dimax = []
    dimax.append(pokemon[0].dimax)
    for i in opponet_pokemon:
        dimax.append(i.dimax)

    e = {
        'pokemon': pokemon[0],
        'p1': pokemon[0],
        'p2': pokemon[1],
        'p3': pokemon[2],
        'p4': pokemon[3],
        'p5': pokemon[4],
        'p6': pokemon[5],
        'op1': opponet_pokemon[0],
        'op2': opponet_pokemon[1],
        'op3': opponet_pokemon[2],
        'op4': opponet_pokemon[3],
        'op5': opponet_pokemon[4],
        'op6': opponet_pokemon[5],
        '1d': damages[0],
        '2d': damages[1],
        '3d': damages[2],
        '4d': damages[3],
        '5d': damages[4],
        '6d': damages[5],
        'role': "攻撃",
        'condition': condition,
        'dimax': dimax,
        'kata_name': kata_name,
        'kata_index': kata_index,
        'attack_kata_name': ["味方"],
        'attack_kata_index': '味方'
    }

    return render(request, 'battle.html', e)


def save_attack_pokemon(request, target):
    attack = request.POST.get("attack_attack_" + target.name)
    if (not attack == "") & (not attack is None):
        target.attack = attack

    special_attack = request.POST.get("attack_special_attack_" + target.name)
    if (not special_attack == "") & (not special_attack is None):
        target.special_attack = special_attack

    speed = request.POST.get("attack_speed_" + target.name)
    if (not speed == "") & (not speed is None):
        target.speed = speed

    attack_boost = request.POST.get("attack_attack_boost_" + target.name)

    if (not attack_boost == "") & (not attack_boost is None):
        target.attack_boost = attack_boost

    special_attack_boost = request.POST.get("attack_special_attack_boost_" + target.name)
    if (not special_attack_boost == "") & (not special_attack_boost is None):
        target.special_attack_boost = special_attack_boost

    speed_boost = request.POST.get("attack_speed_boost_" + target.name)
    if (not speed_boost == "") & (not speed_boost is None):
        target.speed_boost = speed_boost

    ability = request.POST.get("attack_ability_" + target.name)
    if (not ability == "") & (not ability is None):
        target.ability = ability

    item = request.POST.get("attack_item_" + target.name)
    #print(target.name)
    #print(item)
    if (not item == "") & (not item is None):
        target.item = item

    base_stats = request.POST.get("attack_base_stats_" + target.name)
    if (not base_stats == "") & (not base_stats is None):
        target.base_stats = base_stats

    dimax = request.POST.get("attack_dimax_" + target.name)
    if dimax == "on":
        target.dimax = True
    else:
        target.dimax = False

    moves_1 = request.POST.get("attack_moves_1_" + target.name)
    if (not moves_1 == "") & (not moves_1 is None):
        target.moves_1 = moves_1

    moves_2 = request.POST.get("attack_moves_2_" + target.name)
    if (not moves_2 == "") & (not moves_2 is None):
        target.moves_2 = moves_2

    moves_3 = request.POST.get("attack_moves_3_" + target.name)
    if (not moves_3 == "") & (not moves_3 is None):
        target.moves_3 = moves_3

    moves_4 = request.POST.get("attack_moves_4_" + target.name)
    if (not moves_4 == "") & (not moves_4 is None):
        target.moves_4 = moves_4
    target.save()

    return target


def save_defence_pokemon(request, target):
    defense = request.POST.get("defense_" + target.name)
    if (not defense == "") & (not defense is None):
        target.defense = defense

    special_defense = request.POST.get("special_defense_" + target.name)
    if (not special_defense == "") & (not special_defense is None):
        target.special_defense = special_defense

    hp = request.POST.get("hp_" + target.name)
    if (not hp == "") & (not hp is None):
        target.hp = hp

    defense_boost = request.POST.get("defense_boost_" + target.name)

    if (not defense_boost == "") & (not defense_boost is None):
        target.defense_boost = defense_boost

    special_defense_boost = request.POST.get("special_defense_boost_" + target.name)
    if (not special_defense_boost == "") & (not special_defense_boost is None):
        target.special_defense_boost = special_defense_boost

    speed_boost = request.POST.get("speed_boost_" + target.name)
    if (not speed_boost == "") & (not speed_boost is None):
        target.speed_boost = speed_boost

    ability = request.POST.get("ability_" + target.name)
    if (not ability == "") & (not ability is None):
        target.ability = ability

    item = request.POST.get("item_" + target.name)
    if (not item == "") & (not item is None):
        target.item = item

    base_stats = request.POST.get("base_stats_" + target.name)
    if (not base_stats == "") & (not base_stats is None):
        target.base_stats = base_stats

    dimax = request.POST.get("defence_dimax_" + target.name)
    #print(dimax)
    if dimax == "on":
        target.dimax = True
    else:
        target.dimax = False

    target.save()

    return target


def detect_opponent_pokemon(name):
    if Kata_Pokemon.objects.filter(name=name).exists():
        targets = Kata_Pokemon.objects.filter(name=name)
        kata_name = []
        for target in targets:
            Opponent_Pokemon.objects.create(name=target.name, kata=target.kata, ability=target.ability,
                                            item=target.item,
                                            base_stats=target.base_stats,
                                            dimax=False, moves_1=target.moves_1, moves_2=target.moves_2,
                                            moves_3=target.moves_3, moves_4=target.moves_4,
                                            hp_percent="100",
                                            hp=target.hp, attack=target.attack,
                                            defense=target.defense, special_attack=target.special_attack,
                                            special_defense=target.special_defense,
                                            speed=target.speed, attack_boost=target.attack_boost,
                                            defense_boost=target.defense_boost,
                                            special_attack_boost=target.special_attack_boost,
                                            special_defense_boost=target.special_defense_boost,
                                            speed_boost=target.speed_boost)
            kata_name.append(target.kata)


        return [targets[0], kata_name]
    else:
        target = Kata_Pokemon.objects.create(name=name, hp_percent="100", kata="0")
        kata_name = []
        kata_name.append(target.kata)
        Opponent_Pokemon.objects.create(name=target.name, kata=target.kata, ability=target.ability, item=target.item,
                                        base_stats=target.base_stats,
                                        dimax=False, moves_1=target.moves_1, moves_2=target.moves_2,
                                        moves_3=target.moves_3, moves_4=target.moves_4,
                                        hp_percent="100",
                                        hp=target.hp, attack=target.attack,
                                        defense=target.defense, special_attack=target.special_attack,
                                        special_defense=target.special_defense,
                                        speed=target.speed, attack_boost=target.attack_boost,
                                        defense_boost=target.defense_boost,
                                        special_attack_boost=target.special_attack_boost,
                                        special_defense_boost=target.special_defense_boost,
                                        speed_boost=target.speed_boost)
        return [target, kata_name]


def convert_name(japanesename):
    return Convert_Name.objects.get(japanese_name=japanesename).english_name


def convert_moves(japanesename):
    return Convert_Moves.objects.get(japanese_name=japanesename).english_name


def convert_item(japanesename):
    return Convert_Item.objects.get(japanese_name=japanesename).english_name


def convert_seikaku(japanesename):
    if japanesename == None:
        return "serious"
    return Convert_Seikaku.objects.get(japanese_name=japanesename).english_name


def convert_tokusei(japanesename):
    return Convert_Tokusei.objects.get(japanese_name=japanesename).english_name


def load_kata_data():
    with open(r'C:\Users\kohei nakanishi\hello\static\js\kata.csv', encoding="utf-8-sig") as f:
        reader = csv.reader(f)
        Kata_Pokemon.objects.all().delete()
        for row in reader:
            if not row[0] == "\ufeff":
                a = Kata_Pokemon.objects.create(name=row[0], kata=row[1], ability=row[3], item=row[4],
                                                base_stats=row[2],
                                                dimax=False, moves_1=row[11], moves_2=row[12],
                                                moves_3=row[13], moves_4=row[14],
                                                hp_percent="100",
                                                hp=row[5], attack=row[6],
                                                defense=row[7], special_attack=row[9],
                                                special_defense=row[10],
                                                speed=row[8])
                a.save()
