from django.urls import path

from . import views

urlpatterns = [
    # path('', views.index, name='index'),
    path('', views.top, name='top'),
    path('damage/', views.decide, name="decide")
]
