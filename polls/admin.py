from django.contrib import admin
from .models import Pokemon
from .models import Convert_Name
from .models import Convert_Moves
from .models import Convert_Item
from .models import Convert_Tokusei
from .models import Convert_Seikaku
from .models import Kata_Pokemon

# Register your models here.
admin.site.register(Pokemon)
admin.site.register(Convert_Name)
admin.site.register(Convert_Moves)
admin.site.register(Convert_Item)
admin.site.register(Convert_Tokusei)
admin.site.register(Convert_Seikaku)
admin.site.register(Kata_Pokemon)