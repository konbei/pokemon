from copy import deepcopy
from  copy import copy
from ..data import all_move_json
from ...showdown import constants
from .helpers import normalize_name
from .engine.special_effects.items.modify_attack_against import item_modify_attack_against
from .engine.special_effects.items.modify_attack_being_used import item_modify_attack_being_used

import sys

sys.path.append(r'C:\Users\kohei nakanishi\hello\polls')
from polls.models import Convert_Moves

pokemon_type_indicies = {
    'normal': 0,
    'fire': 1,
    'water': 2,
    'electric': 3,
    'grass': 4,
    'ice': 5,
    'fighting': 6,
    'poison': 7,
    'ground': 8,
    'flying': 9,
    'psychic': 10,
    'bug': 11,
    'rock': 12,
    'ghost': 13,
    'dragon': 14,
    'dark': 15,
    'steel': 16,
    'fairy': 17,
    'typeless': 18
}

damage_multipication_array = [[1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 / 2, 0, 1, 1, 1 / 2, 1, 1],
                              [1, 1 / 2, 1 / 2, 1, 2, 2, 1, 1, 1, 1, 1, 2, 1 / 2, 1, 1 / 2, 1, 2, 1, 1],
                              [1, 2, 1 / 2, 1, 1 / 2, 1, 1, 1, 2, 1, 1, 1, 2, 1, 1 / 2, 1, 1, 1, 1],
                              [1, 1, 2, 1 / 2, 1 / 2, 1, 1, 1, 0, 2, 1, 1, 1, 1, 1 / 2, 1, 1, 1, 1],
                              [1, 1 / 2, 2, 1, 1 / 2, 1, 1, 1 / 2, 2, 1 / 2, 1, 1 / 2, 2, 1, 1 / 2, 1, 1 / 2, 1, 1],
                              [1, 1 / 2, 1 / 2, 1, 2, 1 / 2, 1, 1, 2, 2, 1, 1, 1, 1, 2, 1, 1 / 2, 1, 1],
                              [2, 1, 1, 1, 1, 2, 1, 1 / 2, 1, 1 / 2, 1 / 2, 1 / 2, 2, 0, 1, 2, 2, 1 / 2, 1],
                              [1, 1, 1, 1, 2, 1, 1, 1 / 2, 1 / 2, 1, 1, 1, 1 / 2, 1 / 2, 1, 1, 0, 2, 1],
                              [1, 2, 1, 2, 1 / 2, 1, 1, 2, 1, 0, 1, 1 / 2, 2, 1, 1, 1, 2, 1, 1],
                              [1, 1, 1, 1 / 2, 2, 1, 2, 1, 1, 1, 1, 2, 1 / 2, 1, 1, 1, 1 / 2, 1, 1],
                              [1, 1, 1, 1, 1, 1, 2, 2, 1, 1, 1 / 2, 1, 1, 1, 1, 0, 1 / 2, 1, 1],
                              [1, 1 / 2, 1, 1, 2, 1, 1 / 2, 1 / 2, 1, 1 / 2, 2, 1, 1, 1 / 2, 1, 2, 1 / 2, 1 / 2, 1],
                              [1, 2, 1, 1, 1, 2, 1 / 2, 1, 1 / 2, 2, 1, 2, 1, 1, 1, 1, 1 / 2, 1, 1],
                              [0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 1, 1, 2, 1, 1 / 2, 1, 1, 1],
                              [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 1, 1 / 2, 0, 1],
                              [1, 1, 1, 1, 1, 1, 1 / 2, 1, 1, 1, 2, 1, 1, 2, 1, 1 / 2, 1, 1 / 2, 1],
                              [1, 1 / 2, 1 / 2, 1 / 2, 1, 2, 1, 1, 1, 1, 1, 1, 2, 1, 1, 1, 1 / 2, 2, 1],
                              [1, 1 / 2, 1, 1, 1, 1, 2, 1 / 2, 1, 1, 1, 1, 1, 1, 2, 2, 1 / 2, 1, 1],
                              [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]]

SPECIAL_LOGIC_MOVES = {
    "seismictoss": lambda attacker, defender: [int(attacker.level)] if "ghost" not in defender.types else None,
    "nightshade": lambda attacker, defender: [int(attacker.level)] if "normal" not in defender.types else None,
    "superfang": lambda attacker, defender: [int(defender.hp / 2)] if "ghost" not in defender.types else None,
    "naturesmadness": lambda attacker, defender: [int(defender.hp / 2)],
    "finalgambit": lambda attacker, defender: [int(attacker.hp)] if "ghost" not in defender.types else None,
    "endeavor": lambda attacker, defender: [
        int(defender.hp - attacker.hp)] if defender.hp > attacker.hp and "ghost" not in defender.types else None,
    "painsplit": lambda attacker, defender: [defender.hp - (attacker.hp + defender.hp) / 2],
}

TERRAIN_DAMAGE_BOOST = 1.3


def calculate_damage(attacker, defender, move, conditions=None, calc_type='average', dimax=False):

    acceptable_calc_types = ['average', 'max', 'min_max', 'min_max_average', 'all']
    if calc_type not in acceptable_calc_types:
        raise ValueError("{} is not one of {}".format(calc_type, acceptable_calc_types))

    attacking_move = get_move(move)
    a_move=deepcopy(attacking_move)
    if a_move is None:
        raise TypeError("Invalid move: {}".format(move))

    attacking_type = normalize_name(a_move.get(constants.CATEGORY))
    if attacking_type == constants.PHYSICAL:
        attack = constants.ATTACK
        defense = constants.DEFENSE
    elif attacking_type == constants.SPECIAL:
        attack = constants.SPECIAL_ATTACK
        defense = constants.SPECIAL_DEFENSE
    else:
        return None

    try:
        return SPECIAL_LOGIC_MOVES[a_move[constants.ID]](attacker, defender)
    except KeyError:
        pass

    if a_move[constants.BASE_POWER] == 0:
        return [0]

    if conditions is None:
        conditions = {}

    attacking_stats = attacker.calculate_boosted_stats()
    defending_stats = defender.calculate_boosted_stats()

    if dimax:
        a_move = convert_dimax(a_move)
    if not attacker.item == None:
        a_move = item_modify_attack_being_used(item_name=attacker.item, attacking_move=a_move,
                                               attacking_pokemon=attacker, defending_pokemon=defender)
        if attacker.item == "Expert Belt":
            a_move = expertbelt(a_move, attacking_pokemon=attacker, defending_pokemon=defender)

    if not defender.item == "None":
        a_move = item_modify_attack_against(item_name=defender.item, attacking_move=a_move,
                                            attacking_pokemon=attacker, defending_pokemon=defender)

        if defender.item == "Weakness Policy":
            a_move = weaknesspolicy(a_move, attacker, defender)

    if not attacker.base_stats is None:
        a_move = ability_modify_attack_being_used(attacker.ability, a_move, attacker, defender)

    if not defender.base_stats is None:
        a_move = ability_modify_attack_against(defender.ability, a_move, attacker, defender)

    if attacker.ability == 'unaware':
        if defense == constants.DEFENSE:
            defending_stats[defense] = defender.defense
        elif defense == constants.SPECIAL_DEFENSE:
            defending_stats[defense] = defender.special_defense
    if defender.ability == 'unaware':
        if attack == constants.ATTACK:
            attacking_stats[attack] = attacker.attack
        elif defense == constants.SPECIAL_ATTACK:
            attacking_stats[attack] = attacker.special_attack

    defending_types = defender.types
    if a_move[constants.ID] == 'thousandarrows' and 'flying' in defending_types:
        defending_types = copy(defender.types)
        defending_types.remove('flying')
    if a_move[constants.TYPE] == 'ground' and constants.ROOST in defender.volatile_status:
        defending_types = copy(defender.types)
        try:
            defending_types.remove('flying')
        except ValueError:
            pass

    # rock types get 1.5x SPDEF in sand
    try:
        if conditions[constants.WEATHER] == constants.SAND and 'rock' in defender.types:
            defending_stats[constants.SPECIAL_DEFENSE] = int(defending_stats[constants.SPECIAL_DEFENSE] * 1.5)
    except KeyError:
        pass

    damage = int(int((2 * attacker.level) / 5) + 2) * a_move[constants.BASE_POWER]
    damage = int(damage * attacking_stats[attack] / defending_stats[defense])
    damage = int(damage / 50) + 2
    damage *= calculate_modifier(attacker, defender, defending_types, a_move, conditions)

    damage_rolls = get_damage_rolls(damage, calc_type)

    return list(set(damage_rolls))


def convert_dimax(attacking_move):
    print(attacking_move[constants.ID])
    moves = Convert_Moves.objects.get(english_name=attacking_move[constants.NAME])
    a = attacking_move
    if not moves.dimax_power == 0:
        a[constants.BASE_POWER] = moves.dimax_power
        return a
    if a[constants.TYPE] == "poison" or a[constants.TYPE] == "fighting":
        if 10 <= int(a[constants.BASE_POWER]) <= 40:
            a[constants.BASE_POWER] = 70
        elif 45 <= a[constants.BASE_POWER] <= 50:
            a[constants.BASE_POWER] = 75
        elif 55 <= a[constants.BASE_POWER] <= 60:
            a[constants.BASE_POWER] = 80
        elif 65 <= a[constants.BASE_POWER] <= 70:
            a[constants.BASE_POWER] = 85
        elif 75 <= a[constants.BASE_POWER] <= 100:
            a[constants.BASE_POWER] = 90
        elif 110 <= a[constants.BASE_POWER] <= 140:
            a[constants.BASE_POWER] = 95
        elif 150 <= a[constants.BASE_POWER] <= 250:
            a[constants.BASE_POWER] = 100
    else:
        if 10 <= a[constants.BASE_POWER] <= 40:
            a[constants.BASE_POWER] = 90
        elif 45 <= a[constants.BASE_POWER] <= 50:
            a[constants.BASE_POWER] = 100
        elif 55 <= a[constants.BASE_POWER] <= 60:
            a[constants.BASE_POWER] = 110
        elif 65 <= a[constants.BASE_POWER] <= 70:
            a[constants.BASE_POWER] = 120
        elif 75 <= a[constants.BASE_POWER] <= 100:
            a[constants.BASE_POWER] = 130
        elif 110 <= a[constants.BASE_POWER] <= 140:
            a[constants.BASE_POWER] = 140
        elif 150 <= a[constants.BASE_POWER] <= 250:
            a[constants.BASE_POWER] = 150

    return a


def is_super_effective(move_type, defending_pokemon_types):
    multiplier = type_effectiveness_modifier(move_type, defending_pokemon_types)
    return multiplier > 1


def is_not_very_effective(move_type, defending_pokemon_types):
    multiplier = type_effectiveness_modifier(move_type, defending_pokemon_types)
    return multiplier < 1


def calculate_modifier(attacker, defender, defending_types, attacking_move, conditions):
    modifier = 1
    modifier *= type_effectiveness_modifier(attacking_move[constants.TYPE], defending_types)
    modifier *= weather_modifier(attacking_move, conditions.get(constants.WEATHER))
    modifier *= stab_modifier(attacker, attacking_move)
    modifier *= burn_modifier(attacker, attacking_move)
    modifier *= terrain_modifier(attacker, defender, attacking_move, conditions.get(constants.TERRAIN))
    modifier *= volatile_status_modifier(attacking_move, attacker, defender)

    if attacker.ability != 'infiltrator':
        modifier *= light_screen_modifier(attacking_move, conditions.get(constants.LIGHT_SCREEN))
        modifier *= reflect_modifier(attacking_move, conditions.get(constants.REFLECT))
        modifier *= aurora_veil_modifier(conditions.get(constants.AURORA_VEIL))

    return modifier


def get_move(move):
    """
            with open(move_json_location) as f:
            all_move_json = json.load(f)

        p=all_move_json.get(move, None)
        print(p)
        return p
    """
    if isinstance(move, dict):
        return move
    if isinstance(move, str):
        move = normalize_name(move)
        return all_move_json.get(move, None)


def get_damage_rolls(damage, calc_type):
    if calc_type == 'average':
        damage *= 0.925
        return [int(damage)]
    elif calc_type == 'max':
        return [int(damage)]
    elif calc_type == 'min_max':
        return [
            int(damage * 0.85),
            int(damage)
        ]
    elif calc_type == 'min_max_average':
        return [
            int(damage * 0.85),
            int(damage * 0.925),
            int(damage)
        ]
    elif calc_type == 'all':
        return [
            int(damage * 0.85),
            int(damage * 0.86),
            int(damage * 0.87),
            int(damage * 0.88),
            int(damage * 0.89),
            int(damage * 0.90),
            int(damage * 0.91),
            int(damage * 0.92),
            int(damage * 0.93),
            int(damage * 0.94),
            int(damage * 0.95),
            int(damage * 0.96),
            int(damage * 0.97),
            int(damage * 0.98),
            int(damage * 0.99),
            int(damage)
        ]


def type_effectiveness_modifier(attacking_move_type, defending_types):
    modifier = 1
    attacking_type_index = pokemon_type_indicies[normalize_name(attacking_move_type)]
    for pkmn_type in defending_types:
        defending_type_index = pokemon_type_indicies[normalize_name(pkmn_type)]
        modifier *= damage_multipication_array[attacking_type_index][defending_type_index]

    return modifier


def weather_modifier(attacking_move, weather):
    if not isinstance(weather, str):
        return 1

    if weather == constants.SUN and attacking_move[constants.TYPE] == 'fire':
        return 1.5
    elif weather == constants.RAIN and attacking_move[constants.TYPE] == 'water':
        return 1.5
    elif weather == constants.DESOLATE_LAND and attacking_move[constants.TYPE] == 'water':
        return 0
    return 1


def stab_modifier(attacking_pokemon, attacking_move):
    if normalize_name(attacking_move[constants.TYPE]) in [normalize_name(t) for t in attacking_pokemon.types]:
        return 1.5

    return 1


def burn_modifier(attacking_pokemon, attacking_move):
    if constants.BURN == attacking_pokemon.status and normalize_name(
            attacking_move[constants.CATEGORY]) == constants.PHYSICAL:
        return 0.5
    return 1


def light_screen_modifier(attacking_move, light_screen):
    if light_screen and normalize_name(attacking_move[constants.CATEGORY]) == constants.SPECIAL:
        return 0.5
    return 1


def reflect_modifier(attacking_move, reflect):
    if reflect and normalize_name(attacking_move[constants.CATEGORY]) == constants.PHYSICAL:
        return 0.5
    return 1


def aurora_veil_modifier(aurora_veil):
    if aurora_veil:
        return 0.5
    return 1


def terrain_modifier(attacker, defender, attacking_move, terrain):
    if terrain == constants.ELECTRIC_TERRAIN and attacking_move[
        constants.TYPE] == 'electric' and attacker.is_grounded():
        return TERRAIN_DAMAGE_BOOST
    elif terrain == constants.GRASSY_TERRAIN and attacking_move[constants.TYPE] == 'grass' and attacker.is_grounded():
        return TERRAIN_DAMAGE_BOOST
    elif terrain == constants.GRASSY_TERRAIN and attacking_move[constants.ID] == 'earthquake':
        return 0.5
    elif terrain == constants.MISTY_TERRAIN and attacking_move[constants.TYPE] == 'dragon' and defender.is_grounded():
        return 0.5
    elif terrain == constants.PSYCHIC_TERRAIN and attacking_move[
        constants.TYPE] == 'psychic' and attacker.is_grounded():
        return TERRAIN_DAMAGE_BOOST
    elif terrain == constants.PSYCHIC_TERRAIN and attacking_move[constants.PRIORITY] > 0:
        return 0
    return 1


def volatile_status_modifier(attacking_move, attacker, defender):
    modifier = 1
    if 'magnetrise' in defender.volatile_status and attacking_move[constants.TYPE] == 'ground':
        modifier *= 0
    if 'flashfire' in attacker.volatile_status and attacking_move[constants.TYPE] == 'fire':
        modifier *= 1.5
    if 'tarshot' in defender.volatile_status and attacking_move[constants.TYPE] == 'fire':
        modifier *= 2
    return modifier


def expertbelt(attacking_move, attacking_pokemon, defending_pokemon):
    if is_super_effective(attacking_move[constants.TYPE], defending_pokemon.types):
        attacking_move = attacking_move.copy()
        attacking_move[constants.BASE_POWER] *= 1.2
    return attacking_move


def weaknesspolicy(attacking_move, attacking_pokemon, defending_pokemon):
    if attacking_move[constants.CATEGORY] in constants.DAMAGING_CATEGORIES and is_super_effective(
            attacking_move[constants.TYPE], defending_pokemon.types):
        attacking_move = attacking_move.copy()
        attacking_move[constants.BOOSTS] = {
            constants.ATTACK: 2,
            constants.SPECIAL_ATTACK: 2,
        }
    return attacking_move


# sbility atack
# アナライズ
"""
def analytic(attacking_move, attacking_pokemon, defending_pokemon, first_move, weather):
    if not first_move:
        attacking_move = attacking_move.copy()
        attacking_move[constants.BASE_POWER] *= 1.3
    return attacking_move
"""


def adaptability(attacking_move, attacking_pokemon, defending_pokemon, first_move, weather):
    if attacking_move[constants.TYPE] in attacking_pokemon.types:
        attacking_move = attacking_move.copy()
        attacking_move[constants.BASE_POWER] = int(attacking_move[constants.BASE_POWER] * 4 / 3)
    return attacking_move


def aerilate(attacking_move, attacking_pokemon, defending_pokemon, first_move, weather):
    if attacking_move[constants.TYPE] == 'normal':
        attacking_move = attacking_move.copy()
        attacking_move[constants.BASE_POWER] = int(attacking_move[constants.BASE_POWER] * 1.2)
        attacking_move[constants.TYPE] = 'flying'
    return attacking_move


def galvanize(attacking_move, attacking_pokemon, defending_pokemon, first_move, weather):
    if attacking_move[constants.TYPE] == 'normal':
        attacking_move = attacking_move.copy()
        attacking_move[constants.BASE_POWER] = int(attacking_move[constants.BASE_POWER] * 1.2)
        attacking_move[constants.TYPE] = 'electric'
    return attacking_move


def compoundeyes(attacking_move, attacking_pokemon, defending_pokemon, first_move, weather):
    if attacking_move[constants.ACCURACY] is not True:
        attacking_move = attacking_move.copy()
        attacking_move[constants.ACCURACY] *= 1.3
    return attacking_move


def contrary(attacking_move, attacking_pokemon, defending_pokemon, first_move, weather):
    # look at this logic, I want to fucking die
    if attacking_move[constants.TARGET] in constants.MOVE_TARGET_SELF:
        attacking_move = attacking_move.copy()
        if constants.BOOSTS in attacking_move:
            attacking_move[constants.BOOSTS] = attacking_move[constants.BOOSTS].copy()
            for k, v in attacking_move[constants.BOOSTS].items():
                attacking_move[constants.BOOSTS][k] = -1 * v
        if attacking_move[constants.SECONDARY] and constants.BOOSTS in attacking_move[constants.SECONDARY]:
            attacking_move[constants.SECONDARY] = attacking_move[constants.SECONDARY].copy()
            attacking_move[constants.SECONDARY][constants.BOOSTS] = attacking_move[constants.SECONDARY][
                constants.BOOSTS].copy()
            for k, v in attacking_move[constants.SECONDARY][constants.BOOSTS].items():
                attacking_move[constants.SECONDARY][constants.BOOSTS][k] = -1 * v
    elif constants.SELF in attacking_move and constants.BOOSTS in attacking_move[constants.SELF]:
        attacking_move = attacking_move.copy()
        attacking_move[constants.SELF] = attacking_move[constants.SELF].copy()
        attacking_move[constants.SELF][constants.BOOSTS] = attacking_move[constants.SELF][constants.BOOSTS].copy()
        for k, v in attacking_move[constants.SELF][constants.BOOSTS].items():
            attacking_move[constants.SELF][constants.BOOSTS][k] = -1 * v

    elif attacking_move[constants.SECONDARY] and constants.SELF in attacking_move[constants.SECONDARY]:
        if constants.BOOSTS in attacking_move[constants.SECONDARY][constants.SELF]:
            attacking_move = attacking_move.copy()
            attacking_move[constants.SECONDARY] = attacking_move[constants.SECONDARY].copy()
            attacking_move[constants.SECONDARY][constants.SELF] = attacking_move[constants.SECONDARY][
                constants.SELF].copy()
            attacking_move[constants.SECONDARY][constants.SELF][constants.BOOSTS] = \
                attacking_move[constants.SECONDARY][constants.SELF][constants.BOOSTS].copy()
            for k, v in attacking_move[constants.SECONDARY][constants.SELF][constants.BOOSTS].items():
                attacking_move[constants.SECONDARY][constants.SELF][constants.BOOSTS][k] = -1 * v

    return attacking_move


def hustle(attacking_move, attacking_pokemon, defending_pokemon, first_move, weather):
    if attacking_move[constants.CATEGORY] == constants.PHYSICAL:
        attacking_move = attacking_move.copy()
        attacking_move[constants.ACCURACY] *= 0.8
        attacking_move[constants.BASE_POWER] *= 1.5
    return attacking_move


def ironfist(attacking_move, attacking_pokemon, defending_pokemon, first_move, weather):
    if "punch" in attacking_move[constants.FLAGS]:
        attacking_move = attacking_move.copy()
        attacking_move[constants.BASE_POWER] *= 1.2
    return attacking_move


def megalauncher(attacking_move, attacking_pokemon, defending_pokemon, first_move, weather):
    if "pulse" in attacking_move[constants.FLAGS]:
        attacking_move = attacking_move.copy()
        attacking_move[constants.BASE_POWER] *= 1.5
    return attacking_move


def noguard(attacking_move, attacking_pokemon, defending_pokemon, first_move, weather):
    attacking_move = attacking_move.copy()
    attacking_move[constants.ACCURACY] = True
    return attacking_move


def pixilate(attacking_move, attacking_pokemon, defending_pokemon, first_move, weather):
    if attacking_move[constants.TYPE] == 'normal':
        attacking_move = attacking_move.copy()
        attacking_move[constants.BASE_POWER] = int(attacking_move[constants.BASE_POWER] * 1.2)
        attacking_move[constants.TYPE] = 'fairy'
    return attacking_move


def refrigerate(attacking_move, attacking_pokemon, defending_pokemon, first_move, weather):
    if attacking_move[constants.TYPE] == 'normal':
        attacking_move = attacking_move.copy()
        attacking_move[constants.BASE_POWER] = int(attacking_move[constants.BASE_POWER] * 1.2)
        attacking_move[constants.TYPE] = 'ice'
    return attacking_move


def scrappy(attacking_move, attacking_pokemon, defending_pokemon, first_move, weather):
    # this logic is technically wrong, but it at least allows the move to hit
    # for example, a fighting move on ice/ghost should technically be super-effective
    # this logic would make it do neutral damage instead
    if 'ghost' in defending_pokemon.types:
        attacking_move = attacking_move.copy()
        attacking_move[constants.TYPE] = "typeless"
    return attacking_move


def serenegrace(attacking_move, attacking_pokemon, defending_pokemon, first_move, weather):
    if attacking_move[constants.SECONDARY]:
        attacking_move = attacking_move.copy()
        attacking_move[constants.SECONDARY] = attacking_move[constants.SECONDARY].copy()
        attacking_move[constants.SECONDARY][constants.CHANCE] *= 2
        if attacking_move[constants.SECONDARY][constants.CHANCE] > 100:
            attacking_move[constants.SECONDARY][constants.CHANCE] = 100
    return attacking_move


def sheerforce(attacking_move, attacking_pokemon, defending_pokemon, first_move, weather):
    if attacking_move[constants.SECONDARY]:
        attacking_move = attacking_move.copy()
        attacking_move[constants.SECONDARY] = False
        attacking_move[constants.BASE_POWER] *= 1.3
    return attacking_move


def strongjaw(attacking_move, attacking_pokemon, defending_pokemon, first_move, weather):
    if "bite" in attacking_move[constants.FLAGS]:
        attacking_move = attacking_move.copy()
        attacking_move[constants.BASE_POWER] *= 1.5
    return attacking_move


def technician(attacking_move, attacking_pokemon, defending_pokemon, first_move, weather):
    if attacking_move[constants.BASE_POWER] <= 60:
        attacking_move = attacking_move.copy()
        attacking_move[constants.BASE_POWER] *= 1.5
    return attacking_move


def toughclaws(attacking_move, attacking_pokemon, defending_pokemon, first_move, weather):
    if "contact" in attacking_move[constants.FLAGS]:
        attacking_move = attacking_move.copy()
        attacking_move[constants.BASE_POWER] *= 1.3
    return attacking_move


def toxicboost(attacking_move, attacking_pokemon, defending_pokemon, first_move, weather):
    if attacking_move[constants.CATEGORY] == constants.PHYSICAL:
        if attacking_pokemon.status in [constants.POISON, constants.TOXIC]:
            attacking_move = attacking_move.copy()
            attacking_move[constants.BASE_POWER] *= 1.5
    return attacking_move


def hugepower(attacking_move, attacking_pokemon, defending_pokemon, first_move, weather):
    if attacking_move[constants.CATEGORY] == constants.PHYSICAL:
        attacking_move = attacking_move.copy()
        attacking_move[constants.BASE_POWER] *= 2
    return attacking_move


def guts(attacking_move, attacking_pokemon, defending_pokemon, first_move, weather):
    if attacking_pokemon.status is not None and attacking_move[constants.CATEGORY] == constants.PHYSICAL:
        attacking_move = attacking_move.copy()
        attacking_move[constants.BASE_POWER] *= 1.5
        if attacking_pokemon.status == constants.BURN:
            attacking_move[constants.BASE_POWER] *= 2
    return attacking_move


def reckless(attacking_move, attacking_pokeon, defending_pokemon, first_move, weather):
    if constants.RECOIL in attacking_move:
        attacking_move = attacking_move.copy()
        attacking_move[constants.BASE_POWER] *= 1.2
    return attacking_move


def rockhead(attacking_move, attacking_pokeon, defending_pokemon, first_move, weather):
    if constants.RECOIL in attacking_move:
        attacking_move = attacking_move.copy()
        del attacking_move[constants.RECOIL]
    return attacking_move


def parentalbond(attacking_move, attacking_pokeon, defending_pokemon, first_move, weather):
    attacking_move = attacking_move.copy()
    attacking_move[constants.BASE_POWER] *= 1.25
    return attacking_move


def tintedlens(attacking_move, attacking_pokemon, defending_pokemon, first_move, weather):
    if is_not_very_effective(attacking_move[constants.TYPE], defending_pokemon.types):
        attacking_move = attacking_move.copy()
        attacking_move[constants.BASE_POWER] *= 2
    return attacking_move


def skilllink(attacking_move, attacking_pokemon, defending_pokemon, first_move, weather):
    if attacking_move[constants.ID] in ['bulletseed', 'iciclespear', 'pinmissile', 'rockblast', 'tailslap',
                                        'watershuriken']:
        attacking_move = attacking_move.copy()
        attacking_move[constants.BASE_POWER] *= 5
    return attacking_move


def waterbubble(attacking_move, attacking_pokemon, defending_pokemon, first_move, weather):
    if attacking_move[constants.TYPE] == 'water':
        attacking_move = attacking_move.copy()
        attacking_move[constants.BASE_POWER] *= 2

    return attacking_move


def steelworker(attacking_move, attacking_pokemon, defending_pokemon, first_move, weather):
    if attacking_move[constants.TYPE] == 'steel':
        attacking_move = attacking_move.copy()
        attacking_move[constants.BASE_POWER] *= 1.5
    return attacking_move


def neuroforce(attacking_move, attacking_pokemon, defending_pokemon, first_move, weather):
    if is_super_effective(attacking_move[constants.TYPE], defending_pokemon.types):
        attacking_move = attacking_move.copy()
        attacking_move[constants.BASE_POWER] *= 1.25
    return attacking_move


def blaze(attacking_move, attacking_pokemon, defending_pokemon, first_move, weather):
    if attacking_move[constants.TYPE] == 'fire' and (attacking_pokemon.hp / attacking_pokemon.maxhp) <= 1 / 3:
        attacking_move = attacking_move.copy()
        attacking_move[constants.BASE_POWER] *= 1.5
    return attacking_move


def torrent(attacking_move, attacking_pokemon, defending_pokemon, first_move, weather):
    if attacking_move[constants.TYPE] == 'water' and (attacking_pokemon.hp / attacking_pokemon.maxhp) <= 1 / 3:
        attacking_move = attacking_move.copy()
        attacking_move[constants.BASE_POWER] *= 1.5

    return attacking_move


def overgrow(attacking_move, attacking_pokemon, defending_pokemon, first_move, weather):
    if attacking_move[constants.TYPE] == 'grass' and (attacking_pokemon.hp / attacking_pokemon.maxhp) <= 1 / 3:
        attacking_move = attacking_move.copy()
        attacking_move[constants.BASE_POWER] *= 1.5
    return attacking_move


def swarm(attacking_move, attacking_pokemon, defending_pokemon, first_move, weather):
    if attacking_move[constants.TYPE] == 'bug' and (attacking_pokemon.hp / attacking_pokemon.maxhp) <= 1 / 3:
        attacking_move = attacking_move.copy()
        attacking_move[constants.BASE_POWER] *= 1.5
    return attacking_move


def defeatist(attacking_move, attacking_pokemon, defending_pokemon, first_move, weather):
    if attacking_pokemon.hp * 2 <= attacking_pokemon.maxhp:
        attacking_move = attacking_move.copy()
        attacking_move[constants.BASE_POWER] *= 0.5

    return attacking_move


# すなのちから
"""
def sandforce(attacking_move, attacking_pokemon, defending_pokemon, first_move, weather):
    if weather == constants.SAND and attacking_move[constants.TYPE] in ['ground', 'rock', 'steel']:
        attacking_move = attacking_move.copy()
        attacking_move[constants.BASE_POWER] *= 1.3
    return attacking_move
"""


def darkaura(attacking_move, attacking_pokemon, defending_pokemon, first_move, weather):
    if attacking_move[constants.TYPE] == 'dark' and defending_pokemon.ability != 'aurabreak':
        attacking_move = attacking_move.copy()
        attacking_move[constants.BASE_POWER] *= 1.33

    return attacking_move


def fairyaura(attacking_move, attacking_pokemon, defending_pokemon, first_move, weather):
    if attacking_move[constants.TYPE] == 'fairy' and defending_pokemon.ability != 'aurabreak':
        attacking_move = attacking_move.copy()
        attacking_move[constants.BASE_POWER] *= 1.33
    return attacking_move


def prankster(attacking_move, attacking_pokemon, defending_pokemon, first_move, weather):
    if attacking_move[constants.CATEGORY] == constants.STATUS:
        attacking_move = attacking_move.copy()
        attacking_move[constants.ACCURACY] = False
    return attacking_move


def gorillatactics(attacking_move, attacking_pokemon, defending_pokemon, first_move, weather):
    if attacking_move[constants.CATEGORY] == constants.PHYSICAL:
        attacking_move = attacking_move.copy()
        attacking_move[constants.BASE_POWER] *= 1.5
    return attacking_move


def punkrock(attacking_move, attacking_pokemon, defending_pokemon, first_move, weather):
    if constants.SOUND in attacking_move[constants.FLAGS]:
        attacking_move = attacking_move.copy()
        attacking_move[constants.BASE_POWER] *= 1.3
    return attacking_move


def steelyspirit(attacking_move, attacking_pokemon, defending_pokemon, first_move, weather):
    if attacking_move[constants.TYPE] == 'steel':
        attacking_move = attacking_move.copy()
        attacking_move[constants.BASE_POWER] *= 1.5
    return attacking_move


ability_lookup = {
    'steelyspirit': steelyspirit,
    'punkrock': punkrock,
    'gorillatactics': gorillatactics,
    'prankster': prankster,
    'toughclaws': toughclaws,
    'fairyaura': fairyaura,
    'darkaura': darkaura,
    # 'sandforce': sandforce,
    'defeatist': defeatist,
    'swarm': swarm,
    'overgrow': overgrow,
    'torrent': torrent,
    'blaze': blaze,
    'neuroforce': neuroforce,
    'steelworker': steelworker,
    'galvanize': galvanize,
    'waterbubble': waterbubble,
    'adaptability': adaptability,
    # 'analytic': analytic,
    'aerilate': aerilate,
    'compoundeyes': compoundeyes,
    'contrary': contrary,
    'hustle': hustle,
    'ironfist': ironfist,
    'megalauncher': megalauncher,
    'noguard': noguard,
    'pixilate': pixilate,
    'refrigerate': refrigerate,
    'scrappy': scrappy,
    'serenegrace': serenegrace,
    'sheerforce': sheerforce,
    'strongjaw': strongjaw,
    'technician': technician,
    'hugepower': hugepower,
    'purepower': hugepower,
    'reckless': reckless,
    'rockhead': rockhead,
    'guts': guts,
    'parentalbond': parentalbond,
    'toxicboost': toxicboost,
    'tintedlens': tintedlens,
    'skilllink': skilllink
}


def ability_modify_attack_being_used(ability_name, attacking_move, attacking_pokemon, defending_pokemon, first_move="",
                                     weather=""):
    if attacking_pokemon.ability == 'neutralizinggas' or defending_pokemon.ability == 'neutralizinggas':
        return attacking_move
    ability_func = ability_lookup.get(ability_name)
    if ability_func is not None:

        return ability_func(attacking_move, attacking_pokemon, defending_pokemon, first_move, weather)
    else:
        return attacking_move


# ability against
def levitate(attacking_move, attacking_pokemon, defending_pokemon):
    if attacking_move[constants.TYPE] == 'ground' and attacking_move[
        constants.TARGET] in constants.MOVE_TARGET_OPPONENT and attacking_move[constants.ID] != 'thousandarrows':
        attacking_move = attacking_move.copy()
        attacking_move[constants.BASE_POWER] = 0
    return attacking_move


def lightningrod(attacking_move, attacking_pokemon, defending_pokemon):
    if attacking_move[constants.TYPE] == 'electric' and attacking_move[
        constants.TARGET] in constants.MOVE_TARGET_OPPONENT:
        attacking_move = attacking_move.copy()
        attacking_move[constants.ACCURACY] = True
        attacking_move[constants.BASE_POWER] = 0
        attacking_move[constants.TARGET] = constants.NORMAL
        attacking_move[constants.CATEGORY] = constants.STATUS
        attacking_move[constants.BOOSTS] = {
            constants.SPECIAL_ATTACK: 1
        }
    return attacking_move


def stormdrain(attacking_move, attacking_pokemon, defending_pokemon):
    if attacking_move[constants.TYPE] == 'water' and attacking_move[constants.TARGET] in constants.MOVE_TARGET_OPPONENT:
        attacking_move = attacking_move.copy()
        attacking_move[constants.ACCURACY] = True
        attacking_move[constants.BASE_POWER] = 0
        attacking_move[constants.TARGET] = constants.NORMAL
        attacking_move[constants.CATEGORY] = constants.STATUS
        attacking_move[constants.BOOSTS] = {
            constants.SPECIAL_ATTACK: 1
        }
    return attacking_move


def voltabsorb(attacking_move, attacking_pokemon, defending_pokemon):
    if attacking_move[constants.TYPE] == 'electric' and attacking_move[
        constants.TARGET] in constants.MOVE_TARGET_OPPONENT:
        attacking_move = attacking_move.copy()
        attacking_move[constants.ACCURACY] = True
        attacking_move[constants.BASE_POWER] = 0
        attacking_move[constants.HEAL_TARGET] = constants.NORMAL
        attacking_move[constants.CATEGORY] = constants.STATUS
        attacking_move[constants.HEAL] = [
            1,
            4
        ]
    return attacking_move


def waterabsorb(attacking_move, attacking_pokemon, defending_pokemon):
    if attacking_move[constants.TYPE] == 'water' and attacking_move[constants.TARGET] in constants.MOVE_TARGET_OPPONENT:
        attacking_move = attacking_move.copy()
        attacking_move[constants.ACCURACY] = True
        attacking_move[constants.BASE_POWER] = 0
        attacking_move[constants.HEAL_TARGET] = constants.NORMAL
        attacking_move[constants.CATEGORY] = constants.STATUS
        attacking_move[constants.HEAL] = [
            1,
            4
        ]
    return attacking_move


def motordrive(attacking_move, attacking_pokemon, defending_pokemon):
    if attacking_move[constants.TYPE] == 'electric' and attacking_move[
        constants.TARGET] in constants.MOVE_TARGET_OPPONENT:
        attacking_move = attacking_move.copy()
        attacking_move[constants.ACCURACY] = True
        attacking_move[constants.BASE_POWER] = 0
        attacking_move[constants.TARGET] = constants.NORMAL
        attacking_move[constants.CATEGORY] = constants.STATUS
        attacking_move[constants.BOOSTS] = {
            constants.SPEED: 1
        }
    return attacking_move


def sapsipper(attacking_move, attacking_pokemon, defending_pokemon):
    if attacking_move[constants.TYPE] == 'grass' and attacking_move[constants.TARGET] in constants.MOVE_TARGET_OPPONENT:
        attacking_move = attacking_move.copy()
        attacking_move[constants.ACCURACY] = True
        attacking_move[constants.BASE_POWER] = 0
        attacking_move[constants.VOLATILE_STATUS] = None
        attacking_move[constants.TARGET] = constants.NORMAL
        attacking_move[constants.CATEGORY] = constants.STATUS
        attacking_move[constants.BOOSTS] = {
            constants.ATTACK: 1
        }
    return attacking_move


def multiscale(attacking_move, attacking_pokemon, defending_pokemon):
    if defending_pokemon.hp >= defending_pokemon.maxhp:
        attacking_move = attacking_move.copy()
        attacking_move[constants.BASE_POWER] /= 2

    return attacking_move


def thickfat(attacking_move, attacking_pokemon, defending_pokemon):
    if attacking_move[constants.TYPE] in ['fire', 'ice']:
        attacking_move = attacking_move.copy()
        attacking_move[constants.BASE_POWER] /= 2
    return attacking_move


def solidrock(attacking_move, attacking_pokemon, defending_pokemon):
    if is_super_effective(attacking_move[constants.TYPE], defending_pokemon.types):
        attacking_move = attacking_move.copy()
        attacking_move[constants.BASE_POWER] *= (3 / 4)
    return attacking_move


def contrary(attacking_move, attacking_pokemon, defending_pokemon):
    if attacking_move[constants.TARGET] in constants.MOVE_TARGET_OPPONENT:
        attacking_move = attacking_move.copy()
        if constants.BOOSTS in attacking_move:
            attacking_move[constants.BOOSTS] = attacking_move[constants.BOOSTS].copy()
            for k, v in attacking_move[constants.BOOSTS].items():
                attacking_move[constants.BOOSTS][k] = -1 * v
        if attacking_move[constants.SECONDARY] and constants.BOOSTS in attacking_move[constants.SECONDARY]:
            attacking_move[constants.SECONDARY] = attacking_move[constants.SECONDARY].copy()
            attacking_move[constants.SECONDARY][constants.BOOSTS] = attacking_move[constants.SECONDARY][
                constants.BOOSTS].copy()
            for k, v in attacking_move[constants.SECONDARY][constants.BOOSTS].items():
                attacking_move[constants.SECONDARY][constants.BOOSTS][k] = -1 * v

    return attacking_move


def noguard(attacking_move, attacking_pokemon, defending_pokemon):
    attacking_move = attacking_move.copy()
    attacking_move[constants.ACCURACY] = True
    return attacking_move


def flashfire(attacking_move, attacking_pokemon, defending_pokemon):
    # does not account for the 'flashfire' volatile status
    if attacking_move[constants.TYPE] == 'fire':
        attacking_move = attacking_move.copy()
        attacking_move[constants.BASE_POWER] = 0
        attacking_move[constants.STATUS] = None
    return attacking_move


def bulletproof(attacking_move, attacking_pokemon, defending_pokemon):
    if 'bullet' in attacking_move[constants.FLAGS]:
        attacking_move = attacking_move.copy()
        attacking_move[constants.BASE_POWER] = 0
    return attacking_move


def furcoat(attacking_move, attacking_pokemon, defending_pokemon):
    if attacking_move[constants.CATEGORY] == constants.PHYSICAL:
        attacking_move = attacking_move.copy()
        attacking_move[constants.BASE_POWER] *= 0.5
    return attacking_move


def fluffy(attacking_move, attacking_pokemon, defending_pokemon):
    if constants.CONTACT in attacking_move[constants.FLAGS]:
        attacking_move = attacking_move.copy()
        attacking_move[constants.BASE_POWER] *= 0.5
    if attacking_move[constants.TYPE] == 'fire':
        attacking_move = attacking_move.copy()
        attacking_move[constants.BASE_POWER] *= 2
    return attacking_move


def magicbounce(attacking_move, attacking_pokemon, defending_pokemon):
    if attacking_move[constants.FLAGS].get(constants.REFLECTABLE):
        attacking_move = attacking_move.copy()
        attacking_move[constants.TARGET] = constants.SELF

    return attacking_move


def ironbarbs(attacking_move, attacking_pokemon, defending_pokemon):
    if constants.CONTACT in attacking_move[constants.FLAGS]:
        attacking_move = attacking_move.copy()
        attacking_move[constants.HEAL] = [-1, 8]
        attacking_move[constants.HEAL_TARGET] = constants.SELF
    return attacking_move


def roughskin(attacking_move, attacking_pokemon, defending_pokemon):
    if constants.CONTACT in attacking_move[constants.FLAGS]:
        attacking_move = attacking_move.copy()
        attacking_move[constants.HEAL] = [-1, 16]
        attacking_move[constants.HEAL_TARGET] = constants.SELF
    return attacking_move


def wonderguard(attacking_move, attacking_pokemon, defending_pokemon):
    if not is_super_effective(attacking_move[constants.TYPE], defending_pokemon.types):
        attacking_move = attacking_move.copy()
        attacking_move[constants.BASE_POWER] = 0
    return attacking_move


def stamina(attacking_move, attacking_pokemon, defending_pokemon):
    if attacking_move[constants.CATEGORY] in constants.DAMAGING_CATEGORIES:
        attacking_move = attacking_move.copy()
        attacking_move[constants.BOOSTS] = {constants.DEFENSE: 1}

    return attacking_move


def waterbubble(attacking_move, attacking_pokemon, defending_pokemon):
    if attacking_move[constants.TYPE] == 'fire':
        attacking_move = attacking_move.copy()
        attacking_move[constants.BASE_POWER] /= 2

    return attacking_move


def queenlymajesty(attacking_move, attacking_pokemon, defending_pokemon):
    if attacking_move[constants.PRIORITY] > 0:
        attacking_move = attacking_move.copy()
        attacking_move[constants.BASE_POWER] = 0

    return attacking_move


def tanglinghair(attacking_move, attacking_pokemon, defending_pokemon):
    if constants.CONTACT in attacking_move[constants.FLAGS]:
        attacking_move = attacking_move.copy()
        if constants.SELF in attacking_move:
            attacking_move[constants.SELF] = attacking_move[constants.SELF].copy()
        else:
            attacking_move[constants.SELF] = dict()

        if constants.BOOSTS in attacking_move[constants.SELF]:
            attacking_move[constants.SELF][constants.BOOSTS] = attacking_move[constants.SELF][constants.BOOSTS].copy()
        else:
            attacking_move[constants.SELF][constants.BOOSTS] = dict()

        if constants.SPEED in attacking_move[constants.SELF][constants.BOOSTS]:
            attacking_move[constants.SELF][constants.BOOSTS][constants.SPEED] -= 1
        else:
            attacking_move[constants.SELF][constants.BOOSTS][constants.SPEED] = -1

    return attacking_move


def cottondown(attacking_move, attacking_pokemon, defending_pokemon):
    attacking_move = attacking_move.copy()
    if constants.SELF in attacking_move:
        attacking_move[constants.SELF] = attacking_move[constants.SELF].copy()
    else:
        attacking_move[constants.SELF] = dict()

    if constants.BOOSTS in attacking_move[constants.SELF]:
        attacking_move[constants.SELF][constants.BOOSTS] = attacking_move[constants.SELF][constants.BOOSTS].copy()
    else:
        attacking_move[constants.SELF][constants.BOOSTS] = dict()

    if constants.SPEED in attacking_move[constants.SELF][constants.BOOSTS]:
        attacking_move[constants.SELF][constants.BOOSTS][constants.SPEED] -= 1
    else:
        attacking_move[constants.SELF][constants.BOOSTS][constants.SPEED] = -1

    return attacking_move


def marvelscale(attacking_move, attacking_pokemon, defending_pokemon):
    if defending_pokemon.status is not None and attacking_move[constants.CATEGORY] == constants.PHYSICAL:
        attacking_move = attacking_move.copy()
        attacking_move[constants.BASE_POWER] /= 1.5

    return attacking_move


def justified(attacking_move, attacking_pokemon, defending_pokemon):
    if attacking_move[constants.TYPE] == 'dark' and attacking_move[constants.CATEGORY] in constants.DAMAGING_CATEGORIES:
        attacking_move = attacking_move.copy()
        attacking_move[constants.BOOSTS] = {  # damaging moves dont have boosts so dont bother copying
            constants.ATTACK: 1
        }

    return attacking_move


def shielddust(attacking_move, attacking_pokemon, defending_pokemon):
    if constants.SECONDARY in attacking_move:
        attacking_move = attacking_move.copy()
        attacking_move[constants.SECONDARY] = False

    return attacking_move


def defiant(attacking_move, attacking_pokemon, defending_pokemon):
    attacking_move = attacking_move.copy()
    if constants.BOOSTS in attacking_move:
        attacking_move[constants.BOOSTS] = attacking_move[constants.BOOSTS].copy()
        for _ in attacking_move[constants.BOOSTS].copy():
            if constants.ATTACK not in attacking_move[constants.BOOSTS]:
                attacking_move[constants.BOOSTS][constants.ATTACK] = 0
            else:
                attacking_move[constants.BOOSTS][constants.ATTACK] += 2

    elif attacking_move[constants.SECONDARY] and constants.BOOSTS in attacking_move[constants.SECONDARY]:
        attacking_move[constants.SECONDARY] = attacking_move[constants.SECONDARY].copy()
        attacking_move[constants.SECONDARY][constants.BOOSTS] = attacking_move[constants.SECONDARY][
            constants.BOOSTS].copy()
        for _ in attacking_move[constants.SECONDARY][constants.BOOSTS].copy():
            if constants.ATTACK not in attacking_move[constants.SECONDARY][constants.BOOSTS]:
                attacking_move[constants.SECONDARY][constants.BOOSTS][constants.ATTACK] = 2
            else:
                attacking_move[constants.SECONDARY][constants.BOOSTS][constants.ATTACK] += 2

    return attacking_move


def weakarmor(attacking_move, attacking_pokemon, defending_pokemon):
    if constants.PHYSICAL in attacking_move[constants.CATEGORY]:
        attacking_move = attacking_move.copy()
        if constants.BOOSTS in attacking_move:
            attacking_move[constants.BOOSTS] = attacking_move[constants.BOOSTS].copy()
        else:
            attacking_move[constants.BOOSTS] = dict()

        if constants.DEFENSE in attacking_move[constants.BOOSTS]:
            attacking_move[constants.BOOSTS][constants.DEFENSE] -= 1
        else:
            attacking_move[constants.BOOSTS][constants.DEFENSE] = -1

        if constants.SPEED in attacking_move[constants.BOOSTS]:
            attacking_move[constants.BOOSTS][constants.SPEED] += 2
        else:
            attacking_move[constants.BOOSTS][constants.SPEED] = 2

    return attacking_move


def liquidooze(attacking_move, attacking_pokemon, defending_pokemon):
    if constants.DRAIN in attacking_move:
        attacking_move = attacking_move.copy()
        attacking_move[constants.DRAIN] = attacking_move[constants.DRAIN].copy()
        attacking_move[constants.DRAIN][0] *= -1

    return attacking_move


def innerfocus(attacking_move, attacking_pokemon, defending_pokemon):
    if (
            attacking_move[constants.SECONDARY] and
            constants.VOLATILE_STATUS in attacking_move[constants.SECONDARY] and
            attacking_move[constants.SECONDARY][constants.VOLATILE_STATUS] == constants.FLINCH
    ):
        attacking_move = attacking_move.copy()
        attacking_move[constants.SECONDARY] = False

    return attacking_move


def soundproof(attacking_move, attacking_pokemon, defending_pokemon):
    if constants.SOUND in attacking_move[constants.FLAGS]:
        attacking_move = attacking_move.copy()
        attacking_move[constants.ACCURACY] = False

    return attacking_move


def darkaura(attacking_move, attacking_pokemon, defending_pokemon):
    if attacking_move[constants.TYPE] == 'dark' and attacking_pokemon.ability != 'aurabreak':
        attacking_move = attacking_move.copy()
        attacking_move[constants.BASE_POWER] *= 1.33

    return attacking_move


def fairyaura(attacking_move, attacking_pokemon, defending_pokemon):
    if attacking_move[constants.TYPE] == 'fairy' and attacking_pokemon.ability != 'aurabreak':
        attacking_move = attacking_move.copy()
        attacking_move[constants.BASE_POWER] *= 1.33

    return attacking_move


def icescales(attacking_move, attacking_pokemon, defending_pokemon):
    if attacking_move[constants.CATEGORY] == constants.SPECIAL:
        attacking_move = attacking_move.copy()
        attacking_move[constants.BASE_POWER] *= 0.5

    return attacking_move


def punkrock(attacking_move, attacking_pokemon, defending_pokemon):
    if constants.SOUND in attacking_move[constants.FLAGS]:
        attacking_move = attacking_move.copy()
        attacking_move[constants.BASE_POWER] *= 0.5
    return attacking_move


def steamengine(attacking_move, attacking_pokemon, defending_pokemon):
    # duplicated from 'weakarmor'
    if attacking_move[constants.TYPE] in ['fire', 'water']:
        attacking_move = attacking_move.copy()
        if constants.BOOSTS in attacking_move:
            attacking_move[constants.BOOSTS] = attacking_move[constants.BOOSTS].copy()
        else:
            attacking_move[constants.BOOSTS] = dict()

        if constants.SPEED in attacking_move[constants.BOOSTS]:
            attacking_move[constants.BOOSTS][constants.SPEED] += 6
        else:
            attacking_move[constants.BOOSTS][constants.SPEED] = 6

    return attacking_move


ability_lookup = {
    'steamengine': steamengine,
    'punkrock': punkrock,
    'icescales': icescales,
    'fairyaura': fairyaura,
    'darkaura': darkaura,
    'soundproof': soundproof,
    'innerfocus': innerfocus,
    'liquidooze': liquidooze,
    'weakarmor': weakarmor,
    'defiant': defiant,
    'shielddust': shielddust,
    'justified': justified,
    'marvelscale': marvelscale,
    'tanglinghair': tanglinghair,
    'cottondown': cottondown,
    'queenlymajesty': queenlymajesty,
    'waterbubble': waterbubble,
    'stamina': stamina,
    'levitate': levitate,
    'lightningrod': lightningrod,
    'stormdrain': stormdrain,
    'voltabsorb': voltabsorb,
    'waterabsorb': waterabsorb,
    'dryskin': waterabsorb,
    'motordrive': motordrive,
    'sapsipper': sapsipper,
    'multiscale': multiscale,
    'shadowshield': multiscale,
    'thickfat': thickfat,
    'solidrock': solidrock,
    'contrary': contrary,
    'noguard': noguard,
    'flashfire': flashfire,
    'bulletproof': bulletproof,
    'furcoat': furcoat,
    'prismarmor': solidrock,
    'filter': solidrock,
    'fluffy': fluffy,
    'ironbarbs': ironbarbs,
    'wonderguard': wonderguard,
    'roughskin': roughskin,
    'magicbounce': magicbounce
}


def ability_modify_attack_against(ability_name, attacking_move, attacking_pokemon, defending_pokemon):
    if attacking_pokemon.ability == 'neutralizinggas' or defending_pokemon.ability == 'neutralizinggas':
        return attacking_move
    ability_func = ability_lookup.get(ability_name)
    if ability_func is not None:
        return ability_func(attacking_move, attacking_pokemon, defending_pokemon)
    else:
        return attacking_move
